import requests

zspay_token = "SUA API KEY"
bearer = {'Authorization': 'Bearer ' + zspay_token}

def cadastro_estabelecimento(dados):
    r = requests.post('http://api.zsystems.com.br/estabelecimentos', json=dados, headers=bearer)
    return [r, r.status_code]

def cadastro_contaBancaria(dados):
    r = requests.post('http://api.zsystems.com.br/estabelecimentos/contas_bancarias', json=dados, headers=bearer)
    return [r, r.status_code]

def cadastro_transferencia(dados):
    r = requests.post('http://api.zsystems.com.br/transferencias', json=dados, headers=bearer)
    return [r, r.status_code]

def cadastro_cliente(dados):
    r = requests.post('http://api.zsystems.com.br/clientes', json=dados, headers=bearer)
    return [r, r.status_code]

def cadastro_vendas(dados):
    r = requests.post("http://api.zsystems.com.br/vendas", json=dados, headers=bearer)
    return [r, r.status_code]

def cadastro_planos(dados):
    r = requests.post("http://api.zsystems.com.br/planos", json=dados, headers=bearer)
    return [r, r.status_code]

def cadastro_assinaturas(dados):
    r = requests.post('http://api.zsystems.com.br/planos/assinar', json=dados, headers=bearer)
