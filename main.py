from flask import Flask, render_template, redirect, request, url_for
import services.zspay as zspay

app = Flask(__name__)

@app.route('/')
def index():

    if(request.args.get('message')):
        message = request.args.get('message')
    else:
        message = None

    return render_template('home.html', message=message)


@app.route('/estabelecimento/cadastro', methods=['POST'])
def cadastro():
    dados = {}

    dados['tipoEstabelecimentoId'] = request.form['status_empreendedor']
    dados['nome'] = request.form['nome_titular']
    dados['nomeComprovante'] = request.form['nome_estabelecimento']
    dados['email'] = request.form['email_estabelecimento']
    dados['celular'] = request.form['contato_estabelecimento']
    dados['dataNascimento'] = request.form['data_nascimento']
    dados['cpf'] = request.form['cpf']
    dados['categoria'] = request.form['categoria']
    dados['endereco'] = {
        "logradouro": request.form['logradouro_endereco'],
        "numero": request.form['logradouro_numero'],
        "cep": request.form['cep'],
        "cidade": request.form['regiao'].split('-')[0],
        "estado": request.form['regiao'].split('-')[1],
        "complemento": request.form['complemento']
        }
    
    try:
        cadastro = zspay.cadastro_estabelecimento(dados)
        print(cadastro[1])
        print(cadastro[0].json())
        
        if(cadastro[1] == 400):
            message = "Falha ao realizar cadastro de estabelecimento || Dados faltando ou errados"
        elif(cadastro[1] == 200):
            message = "Sucesso ao realizar cadastro de estabelecimento"
        elif(cadastro[1] == 202):
            message = "Este usuário já se encontra cadastrado"
    except:
        message = "Falha ao realizar processamento de cadastro de estabelecimento"
        
    return redirect(url_for('index', message=message))


app.run('127.0.0.1', 8000)

